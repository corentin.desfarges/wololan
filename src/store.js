import AsyncStorage from '@react-native-community/async-storage';
import logger from 'redux-logger';
// import thunkMiddleware from 'redux-thunk'

import { createStore, applyMiddleware } from 'redux';
import { persistStore, persistCombineReducers } from 'redux-persist';

import reducers from "./reducers";

const persistConfig = {
  key: 'wololan',
  storage: AsyncStorage,
  // blacklist: ["modalReducer"]
};

const store = createStore(
  persistCombineReducers(persistConfig, reducers),
  undefined,
  applyMiddleware(/*thunkMiddleware, */logger)
);
const persistor = persistStore(store);

export { store, persistor };
