import * as R from "ramda";

const initialState = {
  list: [],
};

const DeviceReducer = function(previousState = initialState, action) {
  const newState = R.clone(previousState);
  switch (action.type) {
    case "CLEAR_DEVICES":
      return R.assoc("list", initialState.list, newState);
    case "ADD_DEVICE":
      const addingList = newState.list;
      if (!R.find((device) => device.ipAddress === action.item.ipAddress && device.macAddress === action.item.macAddress)(newState.list)) {
        addingList.push(action.item);
      }
      return R.assoc("list", addingList, newState);
    default:
      return previousState;
  }
};

export default DeviceReducer;
