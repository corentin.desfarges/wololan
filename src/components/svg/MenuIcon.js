import React from 'react';
import {SvgXml} from 'react-native-svg';

const MenuIcon = ({width = 50, height = 50, fill = 'white'}) => {
  return (
    <SvgXml
      xml={`<svg xmlns="http://www.w3.org/2000/svg" height="24" viewBox="0 0 24 24" width="24" fill="${fill}"><path d="M0 0h24v24H0z" fill="none"/><path d="M3 18h18v-2H3v2zm0-5h18v-2H3v2zm0-7v2h18V6H3z"/></svg>`}
      width={width}
      height={height}
    />
  );
};

export default MenuIcon;
