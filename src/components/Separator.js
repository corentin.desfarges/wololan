import React from 'react';
import {
  StyleSheet,
  View,
} from 'react-native';

const Separator = () => {
  return (
    <View style={styles.separator} />
  );
};

const styles = StyleSheet.create({
  separator: {
    width: '100%',
    borderTopWidth: 2,
    borderColor: '#3cb39d',
    marginTop: 20,
    marginBottom: 20,
  },
});

export default Separator;
