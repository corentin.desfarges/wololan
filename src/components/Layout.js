import React, {useEffect, useState} from 'react';
import {
  Image,
  TouchableOpacity,
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  TextInput,
  StatusBar,
} from 'react-native';

import Header from "./Header";

const Layout = ({children, title, subTitle, navigation}) => {
  return (
    <>
      <StatusBar barStyle="dark-content"/>
      <SafeAreaView style={styles.container}>
        <ScrollView
          contentInsetAdjustmentBehavior="automatic"
          contentContainerStyle={{flexGrow: 1}}
          style={styles.scrollView}>
          <Header
            title={title}
            navigation={navigation}
          />
          <View style={styles.main}>
            <Text style={styles.h2}>{subTitle}</Text>
            {children}
          </View>
        </ScrollView>
      </SafeAreaView>
    </>
  );
};

const styles = StyleSheet.create({
  container: {
    display: 'flex',
    flex: 1,
    color: 'white',
  },
  scrollView: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: '#0f2027',
  },
  h2: {
    fontFamily: 'Nunito-Bold',
    fontSize: 22,
    color: 'white',
    marginBottom: 20,
    textAlign: 'center',
  },
  main: {
    flex: 1,
    padding: 20,
    paddingTop: 50,
    borderTopRightRadius: 30,
    borderTopLeftRadius: 30,
    backgroundColor: '#203740',
    alignItems: 'center',
    flexDirection: 'column',
  },
});

export default Layout;
