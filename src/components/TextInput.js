import React, {useState, useRef} from 'react';

import {StyleSheet, TextInput, TouchableOpacity} from "react-native";

const WTextInput = ({style, placeholder, placeholderTextColor, value, keyboardType, onChangeText, onSubmitEditing}) => {
  const inputRef = useRef();
  const [focused, setFocused] = useState(false);
  return (
    <TouchableOpacity onFocus={() => {
      setFocused(true);
      inputRef.current.focus();
    }}>
      <TextInput
        ref={inputRef}
        style={[style, styles.input, focused ? styles.focused : {}]}
        placeholder={placeholder}
        placeholderTextColor={placeholderTextColor}
        value={value}
        keyboardType={keyboardType}
        onChangeText={onChangeText}
        onSubmitEditing={onSubmitEditing}
        onFocus={() => setFocused(true)}
        onBlur={() => setFocused(false)}
      />
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  input: {
    borderTopLeftRadius: 5,
    borderTopRightRadius: 5,
    padding: 5,
    marginTop: 10,
    marginBottom: 10,
    fontWeight: 'normal',
    fontFamily: 'Nunito-Regular',
    fontSize: 24,
    textAlign: 'center',
    color: 'white',
    borderBottomWidth: 1,
    borderBottomColor:  '#3cb39d',
    backgroundColor: '#0f2027',
  },
  focused: {
    backgroundColor: '#0f2027',
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    borderBottomWidth: 1,
    borderBottomColor: 'white',
    elevation: 5,
  },
});

export default WTextInput;
