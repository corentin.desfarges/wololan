import React, {useState} from 'react';
import {
  Modal,
  Image,
  Linking,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import ImageZoomViewer from 'react-native-image-zoom-viewer';

import Layout from "../Layout";

const Issue = ({question, response}) => {
  const [collapsed, toggleCollapsed] = useState(true);
  return (
    <TouchableOpacity
      activeOpacity={1}
      onPress={() => toggleCollapsed(!collapsed)}
    >
      <View style={styles.issue}>
        <Text style={styles.h3}>{question}</Text>
        <View style={[styles.response, collapsed ? {display: 'none'} : {}]}>
          {response}
        </View>
      </View>
    </TouchableOpacity>
  );
};

const ImageViewer = ({images}) => {
  const [displayed, toggleDisplayed] = useState(false);
  return (
    <TouchableOpacity
      activeOpacity={1}
      onPress={() => toggleDisplayed(!displayed)}
    >
      <Image source={images[0].props.source} style={styles.image}/>
      <Modal visible={displayed} transparent={true}>
        <ImageZoomViewer
          imageUrls={images}
          enableSwipeDown={true}
          onCancel={() => toggleDisplayed(false)}
        />
      </Modal>
    </TouchableOpacity>
  );
};

const HyperLink = ({url, text}) => {
  return (
    <Text style={[styles.p, styles.hyperLink]} onPress={() => Linking.openURL(url)}>
      > {text || url}
    </Text>
  )
}

const Help = ({navigation}) => {
  return (
    <Layout title={"Besoin d'aide ?"} subTitle={"FAQ"} navigation={navigation}>
      <View>
        <Issue
          question={"Comment trouver mon adresse IP et mon adresse physique ? (Windows)"}
          response={<>
            <Text style={styles.p}>
              Pour trouver les adresses IP et physique de l'appareil à allumer, lancez l'exécuteur de commande (Ctrl + R) et
              tapez :
            </Text>
            <Text style={[styles.p, styles.highlight]}>
              cmd /k ipconfig /all
            </Text>
            <Text style={styles.p}>
              Appuyez sur "OK" puis identifiez votre carte réseau (généralement Ethernet, Wi-Fi...).
            </Text>
            <Text style={[styles.p, styles.strong]}>
              L'adresse physique devrait ressembler à XX-XX-XX-XX-XX-XX.
            </Text>
            <Text style={[styles.p, styles.strong]}>
              L'adresse IP devrait ressembler à 192.168.XX.XX
            </Text>
            <ImageViewer images={[
              {props: {source: require('../../assets/img/helpAddress.png')}},
            ]}/>
          </>}
        />
        <Issue
          question={"Comment trouver mon adresse IP et mon adresse physique ? (autres plateformes)"}
          response={<>
            <Text style={styles.p}>
              Nous vous invitons à suivre ce tutoriel détaillé :
            </Text>
            <HyperLink
              text={'Trouver ses adresses IP et physique'}
              url={'https://le-routeur-wifi.com/adresse-ip-mac'}
            />
          </>}
        />
        <Issue
          question={"Je pense avoir saisi les bons paramètres, mais ça ne fonctionne pas. Pourquoi ?"}
          response={<>
            <Text style={styles.p}>
              Cette application repose sur l'envoie de "Magic Packet" par le réseau. L'appareil à allumer doit être sur le même réseau que le téléphone ou la tablette qui vous sert de télécommande.
            </Text>
            <Text style={styles.p}>
              Si l'appareil à réveiller est compatible, la fonctionnalité doit être configurée au niveau de votre BIOS et de votre carte réseau (via votre système d'exploitation).
            </Text>
            <Text style={styles.p}>
              Puisqu'il existe une multitudes de configurations, voici quelques liens expliquant le paramétrage :
            </Text>
            <HyperLink
              text={'Cartes mères Intel'}
              url={'https://www.intel.fr/content/www/fr/fr/support/articles/000006002/boards-and-kits/desktop-boards.html'}
            />
            <HyperLink
              text={'Appareils sous Linux'}
              url={'https://doc.ubuntu-fr.org/wakeonlan'}
            />
          </>}
        />
      </View>
    </Layout>
  );
};

const styles = StyleSheet.create({
  issue: {
    padding: 15,
    borderRadius: 10,
    marginBottom: 10,
    backgroundColor: '#224a53',
  },
  h3: {
    fontFamily: 'Nunito-Bold',
    fontSize: 17,
    color: 'white',
  },
  p: {
    marginTop: 5,
    marginBottom: 5,
    fontFamily: 'Nunito-Light',
    fontSize: 17,
    color: 'white',
  },
  strong: {
    fontFamily: 'Nunito-Bold',
  },
  highlight: {
    marginLeft: 10,
    fontFamily: 'Nunito-Black',
    color: '#3cb39d',
  },
  image: {
    height: 100,
    width: '100%',
  },
  hyperLink: {
    fontFamily: 'Nunito-Bold',
    color: '#3cb39d',
  },
});

export default Help;
