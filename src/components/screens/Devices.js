import React from 'react';
import {
  StyleSheet,
  View,
  Text, TouchableOpacity,
} from 'react-native';
import {useDispatch, useSelector} from "react-redux";

import Layout from "../Layout";
import Separator from "../Separator";
import HelpBlock from "../HelpBlock";

const Devices = ({navigation}) => {
  const dispatch = useDispatch();
  const devices = useSelector(s => s.device.list);

  return (
    <Layout title={"Mes appareils"} subTitle={devices.length > 0 ? "Vos appareils enregistrés" : "Aucun appareil enregistré"} navigation={navigation}>
      <View style={styles.container}>
        {devices.length > 0 &&
        <>
          <View style={styles.historyBlock}>
            {devices.map(({ipAddress, macAddress}) => {
              return (
                <TouchableOpacity
                  key={Math.random()}
                  style={styles.historyButton}
                  onPress={() => {
                    navigation.navigate('Allumer un appareil', {
                      ipAddress,
                      macAddress,
                    });
                  }}
                >
                  <Text style={styles.historyButtonText1}>Démarrer l'appareil</Text>
                  <Text style={styles.historyButtonText2}>{ipAddress} / {macAddress}</Text>
                </TouchableOpacity>);
            })}
            <TouchableOpacity
              style={styles.clearButton}
              onPress={() => {
                dispatch({
                  type: "CLEAR_DEVICES"
                });
              }}>
              <Text style={styles.historyButtonText1}>Supprimer les appareils</Text>
            </TouchableOpacity>
          </View>
          <Separator/>
          <HelpBlock />
        </>
        }
        {devices.length === 0 &&
        <View>
          <Text style={styles.p}>Vous n'avez aucun appareil enregistré actuellement. Rendez-vous dans le menu "Allumer un appareil" pour démarrer !</Text>
          <Separator/>
          <HelpBlock />
        </View>
        }
      </View>
    </Layout>
  );
};

const styles = StyleSheet.create({
  p: {
    textAlign: 'center',
    fontFamily: 'Nunito-Regular',
    fontSize: 17,
    color: 'white',
  },
  container: {
    width: '100%',
    height: '100%',
  },
  historyBlock: {
    width: '100%',
    flex: 0,
  },
  historyButton: {
    marginTop: 5,
    padding: 10,
    borderRadius: 5,
    backgroundColor: '#3cb39d',
    alignItems: 'center',
  },
  historyButtonText1: {
    fontFamily: 'Nunito-Bold',
    fontSize: 18,
    color: 'white',
    fontWeight: "bold",
  },
  historyButtonText2: {
    fontFamily: 'Nunito-Light',
    fontSize: 17,
    color: 'white',
  },
  clearButton: {
    marginTop: 20,
    marginBottom: 10,
    padding: 10,
    borderRadius: 5,
    backgroundColor: '#ff9150',
    alignItems: 'center',
  },
});

export default Devices;
