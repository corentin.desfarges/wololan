import React, {useEffect, useState} from 'react';
import {
  Image,
  TouchableOpacity,
  StyleSheet,
  View,
  Text,
} from 'react-native';
import {useDispatch, useSelector} from "react-redux";

import Power from '../svg/PowerIcon';
import Failed from '../svg/FailedIcon';
import Success from '../svg/SuccessIcon';
import Save from '../svg/SaveIcon';

import KotlinWol from '../../KotlinWol';
import Layout from "../Layout";
import Button from "../Button";
import TextInput from "../TextInput";
import Separator from "../Separator";
import HelpBlock from "../HelpBlock";

const maxPingAttempts = 4;

const StartDevice = ({navigation, route}) => {
  const dispatch = useDispatch();
  const params = route.params || {};
  const [ipAddress, setIpAddress] = useState('');
  const [macAddress, setMacAddress] = useState('');
  const [pingStatus, setPingStatus] = useState();
  const [macAddressRequired, setMacAddressRequired] = useState(false);
  const [magicPacketSent, setMagicPacketSent] = useState(false);
  const [startButtonDisabled, setStartButtonDisabled] = useState(true);

  const [deviceSaved, setDeviceSaved] = useState(false);

  const devices = useSelector(s => s.device.list);

  const validateIpAddress = (address) => {
    return /^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/.test(address);
  };

  const validateMacAddress = (address) => {
    return /^[0-9a-f]{1,2}([.:-])(?:[0-9a-f]{1,2}\1){4}[0-9a-f]{1,2}$/i.test(address);
  };

  const ping = (pingAttempt = 1) => {
    setPingStatus('WAITING');
    KotlinWol.sendPingRequest(
      ipAddress,
      5,
      (mac) => {
        if (mac) {
          setMacAddress(mac);
        }
        setPingStatus('PING_OK');
      },
      () => {
        if (pingAttempt >= maxPingAttempts) {
          setPingStatus('PING_KO');
        } else {
          ping(pingAttempt + 1);
        }
      },
    );
  };

  const saveDevice = (ipAddress, macAddress) => {
    dispatch({
      type: "ADD_DEVICE",
      item: {
        ipAddress,
        macAddress,
      }
    });
  };

  const sendMagicPacket = (ipAddress, macAddress) => {
    KotlinWol.sendMagicPacket(
      ipAddress,
      macAddress,
      () => {
        setMagicPacketSent(true);
        return ping();
      },
      () => {
        return setPingStatus('PING_KO');
      },
    )
  };

  const handlePress = () => {
    setMagicPacketSent(false);
    KotlinWol.findMacAddressFromIpAddress(
      ipAddress,
      (macAddress) => {
        setMacAddress(macAddress);
        return sendMagicPacket(ipAddress, macAddress);
      },
      (error) => {
        setMacAddressRequired(error === 'MAC_ADDRESS_NOT_FOUND');
        if (macAddress.length > 0) {
          return sendMagicPacket(ipAddress, macAddress);
        }
      },
    );
  };

  useEffect(() => {
    if (params.ipAddress) {
      setIpAddress(params.ipAddress);
    }
    if (params.macAddress) {
      setMacAddress(params.macAddress);
      setMacAddressRequired(true);
    }
  }, [params.ipAddress, params.macAddress]);

  // display mac address input if magic packet has been sent but no ping response received
  useEffect(() => {
    if (magicPacketSent && pingStatus === 'PING_KO') {
      setMacAddressRequired(true);
    }
  }, [magicPacketSent, pingStatus]);

  useEffect(() => {
    if (!macAddressRequired) {
      setMacAddress('');
    }
  }, [ipAddress, macAddressRequired]);

  useEffect(() => {
    setDeviceSaved(false);
    devices.forEach((device) => {
      if (device.ipAddress === ipAddress && device.macAddress === macAddress) {
        setDeviceSaved(true);
      }
    })
  }, [devices, ipAddress, macAddress]);

  useEffect(() => {
    setStartButtonDisabled(
      !validateIpAddress(ipAddress)
      || (macAddress.length > 0 && !validateMacAddress(macAddress))
      || pingStatus === "WAITING")
  }, [ipAddress, macAddress, pingStatus]);

  useEffect(() => {
    setDeviceSaved(false);
    setPingStatus(null);
  }, [ipAddress, macAddress]);

  return (
    <Layout title={"Démarrer"} subTitle={"Appareil à démarrer"} navigation={navigation}>
      <>
        <View style={styles.startBlock}>
          <Text style={styles.label}>Adresse IP :</Text>
          <TextInput
            placeholder={"Adresse IP"}
            placeholderTextColor={'#d6d6d6'}
            value={ipAddress}
            keyboardType={"numeric"}
            onChangeText={setIpAddress}
            onSubmitEditing={handlePress}
          />
          {macAddressRequired &&
          <>
            <Text style={styles.label}>Adresse physique :</Text>
            <TextInput
              placeholder={"Adresse physique"}
              placeholderTextColor={'#203740'}
              value={macAddress}
              onChangeText={setMacAddress}
              onSubmitEditing={handlePress}
            />
          </>
          }
          <Button style={startButtonDisabled ? styles.startButtonDisabled : styles.startButton} onPress={handlePress} disabled={startButtonDisabled}>
            <Power width={35} height={35} fill={startButtonDisabled ? '#a9a9a9' : '#3cb39d'}/>
            <Text style={startButtonDisabled ? styles.startButtonTextDisabled : styles.startButtonText}>Démarrer</Text>
          </Button>

          <View style={styles.statusBlock}>
            {pingStatus === 'WAITING' && (
              <>
                <Image source={require('../../assets/img/spinner.gif')} style={styles.spinner}/>
                <Text style={styles.loadingText}>Démarrage cours</Text>
              </>
            )}
            {pingStatus === 'PING_OK' && (
              <>
                <Success width={25} height={25} fill={'#fff'}/>
                <Text style={styles.successText}>Appareil allumé !</Text>
              </>
            )}
            {macAddressRequired && macAddress.length === 0 &&
            <>
              <Failed width={25} height={25} fill={'orange'}/>
              <Text style={styles.failedText}>Adresse physique requise</Text>
            </>
            }
            {magicPacketSent && pingStatus === 'PING_KO' && (
              <>
                <Success width={25} height={25} fill={'white'}/>
                <Text style={styles.loadingText}>Demande envoyée</Text>
              </>
            )}
          </View>
        </View>
        {(pingStatus === 'PING_OK' || (magicPacketSent && pingStatus === 'PING_KO')) && !deviceSaved && (
          <View style={styles.saveBlock}>
            <>
              <TouchableOpacity style={styles.saveButton} onPress={() => saveDevice(ipAddress, macAddress)}>
                <Save width={25} height={25} fill={'#3cb39d'}/>
                <Text style={styles.saveButtonText}>Enregistrer cet appareil</Text>
              </TouchableOpacity>
            </>
          </View>
        )}
        {pingStatus === 'PING_OK' && deviceSaved && (
          <View style={styles.saveBlock}>
            <Text style={styles.saveButtonText}>Appareil enregistré</Text>
          </View>
        )}
        <Separator/>
        <HelpBlock />
      </>
    </Layout>
  );
};

const styles = StyleSheet.create({
  startBlock: {
    width: '100%',
    flex: 0,
    marginTop: 5,
  },
  label: {
    fontFamily: 'Nunito-Regular',
    fontSize: 18,
    color: 'white',
  },
  startButton: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    borderWidth: 2,
    borderRadius: 5,
    padding: 5,
    borderColor: '#3cb39d',
    backgroundColor: 'rgba(0,0,0,0.15)',
  },
  startButtonDisabled: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    borderWidth: 2,
    borderRadius: 5,
    padding: 5,
    borderColor: '#a9a9a9',
    backgroundColor: 'rgba(0,0,0,0.15)',
  },
  startButtonText: {
    fontSize: 25,
    marginLeft: 10,
    fontFamily: 'Nunito-Bold',
    color: '#3cb39d',
  },
  startButtonTextDisabled: {
    fontSize: 25,
    marginLeft: 10,
    fontFamily: 'Nunito-Bold',
    color: '#a9a9a9',
  },
  saveButton: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 5,
    padding: 5,
    backgroundColor: 'rgba(0,0,0,0.15)',
  },
  saveButtonText: {
    fontFamily: 'Nunito-Bold',
    fontSize: 17,
    marginLeft: 5,
    color: '#3cb39d',
  },
  statusBlock: {
    flex: 1,
    marginTop: 20,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    padding: 5,
  },
  failedText: {
    marginLeft: 10,
    fontFamily: 'Nunito-Bold',
    fontSize: 20,
    color: 'orange',
  },
  successText: {
    marginLeft: 10,
    fontFamily: 'Nunito-Bold',
    fontSize: 20,
    color: '#fff',
  },
  loadingText: {
    marginLeft: 10,
    fontFamily: 'Nunito-Bold',
    fontSize: 20,
    color: '#fff',
  },
  spinner: {
    width: 35,
    height: 35,
  },
});

export default StartDevice;
