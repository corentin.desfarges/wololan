import React  from 'react';
import {
  StyleSheet,
  Text,
  View,
} from 'react-native';

const HelpBlock = () => {
  return (
    <View>
      <Text style={styles.helpTitle}>Besoin d'aide ?</Text>
      <Text style={styles.helpText}>Consultez la rubrique "Aide" du menu</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  helpTitle: {
    fontFamily: 'Nunito-Bold',
    fontSize: 22,
    marginLeft: 5,
    marginBottom: 15,
    color: '#3cb39d',
    textAlign: 'center',
  },
  helpText: {
    fontFamily: 'Nunito-Regular',
    fontSize: 17,
    marginLeft: 5,
    color: '#fff',
    textAlign: 'center',
  },
});

export default HelpBlock;
