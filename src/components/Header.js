import React, {useEffect, useState} from 'react';
import {
  StyleSheet,
  View,
  Text,
} from 'react-native';

import Menu from './svg/MenuIcon';
import Button from "./Button";

const Header = ({title, navigation}) => {
  return (
    <>
      <View style={styles.header}>
        <Button onPress={() => navigation.openDrawer()}>
          <Menu width={35} height={35}/>
        </Button>
        <Text style={styles.h1}>{title}</Text>
      </View>
    </>
  );
};

const styles = StyleSheet.create({
  header: {
    flex: 0,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start',
    padding: 10,
    paddingTop: 30,
    paddingBottom: 30,
  },
  h1: {
    marginLeft: 20,
    fontFamily: 'Nunito-Black',
    fontSize: 25,
    color: 'white',
  },
});

export default Header;
