import 'react-native-gesture-handler';
import React from 'react';
import { createDrawerNavigator } from '@react-navigation/drawer';
import { NavigationContainer } from '@react-navigation/native';

import StartDevice from "./screens/StartDevice";
import {Text, View, StyleSheet} from "react-native";
import Help from "./screens/Help";
import FullLogo from "./svg/LogoIcon";
import Devices from "./screens/Devices";
import Button from "./Button";

const {Navigator, Screen} = createDrawerNavigator();

const DrawerMenu = () => {
  return (
    <NavigationContainer>
      <Navigator
        drawerType="slide"
        drawerContent={({state, navigation}) => (
          <View>
            <View style={styles.logo}>
              <FullLogo width={200} height={75}/>
              <Text style={styles.appSubTitle}>Wake up your devices</Text>
            </View>
            {state.routes.map((route) => (
              <Button
                onPress={() => navigation.navigate(route.name)}
                style={styles.item}
                key={route.name}
              >
                <Text key={route.key} style={styles.label}>{route.name}</Text>
              </Button>
            ))}
          </View>
        )}
        drawerStyle={styles.drawer}
      >
          <Screen  itemStyle={styles.item} name="Allumer un appareil" component={StartDevice} />
          <Screen name="Mes appareils" component={Devices} />
          <Screen name="Aide" component={Help} />
      </Navigator>
    </NavigationContainer>
  );
};

const styles = StyleSheet.create({
  drawer: {
    backgroundColor: '#203740'
  },
  logo: {
    alignItems: 'center',
    margin: 20,
  },
  appSubTitle: {
    fontFamily: 'Nunito-Bold',
    fontSize: 15,
    color: 'white',
  },
  label: {
    fontFamily: 'Nunito-Regular',
    fontSize: 18,
    color: 'white',
  },
  item: {
    marginLeft: 10,
    marginRight: 10,
    marginTop: 5,
    padding: 10,
    backgroundColor: '#213d46'
  },
});

export default DrawerMenu;
