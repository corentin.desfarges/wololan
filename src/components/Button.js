import React, {useState} from 'react';

import {TouchableOpacity, StyleSheet} from "react-native";

const Button = ({onPress, style, children}) => {
  const [focused, setFocused] = useState(false);
  return (
    <TouchableOpacity
      onPress={onPress}
      style={[focused ? styles.focused : {}, style]}
      onFocus={() => setFocused(true)}
      onBlur={() => setFocused(false)}
    >
      {children}
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  focused: {
    backgroundColor: '#0f2027',
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
});

export default Button;
