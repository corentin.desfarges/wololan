package fr.corentindesfarges.wololan.KotlinWol

import android.content.Intent
import com.facebook.react.bridge.Callback;
import com.facebook.react.bridge.ReactApplicationContext
import com.facebook.react.bridge.ReactContextBaseJavaModule
import com.facebook.react.bridge.ReactMethod

import java.net.DatagramPacket
import java.net.DatagramSocket
import java.net.InetAddress
import java.io.BufferedReader
import java.io.File
import java.io.FileReader
import java.io.InputStream
import java.io.IOException

public class DoMagic(reactContext: ReactApplicationContext) : ReactContextBaseJavaModule(reactContext) {
    override fun getName(): String {
        return "KotlinDoMagic"
    }

    private fun getMacAddressFromIpAddress(ipAddress: String): String {
        try {
            val inputStream: InputStream = File("/proc/net/arp").inputStream()
            val reader = inputStream.bufferedReader()
            var line: String = ""
            val lineList = mutableListOf<String>()

            inputStream.bufferedReader().useLines { lines ->
                lines.forEach {
                    line = it;

                    val regex = Regex("(^(?:[0-9]{1,3}\\.){3}[0-9]{1,3}).*(..:..:..:..:..:..)")
                    val matchResult = regex.find(line);
                    if (matchResult != null) {
                        val (ip, mac) = matchResult!!.destructured
                        if (ipAddress == ip) {
                            return mac
                        }
                    }
                }
            }
            reader.close()
            return "";
        } catch (e: IOException) {
            e.printStackTrace()
            return ""
        }
    }

    private fun getMacBytes(macStr: String): ByteArray? {
        val bytes = ByteArray(6)
        val hex = macStr.split("-", ":").toTypedArray()
        require(hex.size == 6) { "Invalid MAC address." }
        try {
            for (i in 0..5) {
                bytes[i] = hex[i].toInt(16).toByte()
            }
        } catch (e: NumberFormatException) {
            throw IllegalArgumentException("Invalid hex digit in MAC address.")
        }
        return bytes
    }

    @ReactMethod
    fun sendPingRequest(ipAddress: String, timeout: Int, successCallback: Callback, errorCallback: Callback) {
        try {
            Thread({
                val runtime: Runtime = Runtime.getRuntime()
                val mIpAddrProcess: Process = runtime.exec("/system/bin/ping -c 1 -w " + timeout + " " + ipAddress)
                val mExitValue: Int = mIpAddrProcess.waitFor()
                if (mExitValue == 0) {
                    successCallback()
                } else {
                    errorCallback("nooopppe :(")
                }
            }).start()
        } catch (e: Exception) {
            errorCallback("nooopppe :(")
        }
    }

    @ReactMethod
    fun findMacAddressFromIpAddress(ipAddress: String, successCallback: Callback, errorCallback: Callback) {
        if (ipAddress.length == 0)
            return errorCallback("IP_ADDRESS_MISSING")
        var macAddress: String = getMacAddressFromIpAddress(ipAddress)
        if (macAddress.length > 0) {
            return successCallback(macAddress);
        }
        return errorCallback("MAC_ADDRESS_NOT_FOUND");
    }

    @ReactMethod
    fun sendMagicPacket(ipAddress: String, macAddress: String, successCallback: Callback, errorCallback: Callback) {
        val PORT: Int = 9;
        if (ipAddress.length == 0)
            return errorCallback("IP_ADDRESS_MISSING")
        if (macAddress.length == 0 || macAddress === "00:00:00:00:00:00")
            return errorCallback("MAC_ADDRESS_MISSING")

        try {
            val macBytes = getMacBytes(macAddress)
            val bytes = ByteArray(6 + 16 * macBytes!!.size)
            for (i in 0..5) {
                bytes[i] = 0xff.toByte()
            }
            var i = 6
            while (i < bytes.size) {
                System.arraycopy(macBytes!!, 0, bytes, i, macBytes!!.size)
                i += macBytes!!.size
            }
            val address: InetAddress = InetAddress.getByName(ipAddress)
            val packet = DatagramPacket(bytes, bytes.size, address, PORT)
            val socket = DatagramSocket()
            Thread({
                socket.send(packet)
                socket.close()
                successCallback(macAddress);
            }).start()
        } catch (e: Exception) {
            errorCallback();
        }
    }
}

